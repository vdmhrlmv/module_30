
async def test_get_something(ac):
    response = await ac.get('/')
    assert response.status_code == 200
    assert response.json() == {'Service name': 'recipe_book'}


async def test_add_recipe(ac):
    response = await ac.post(
        "/recipe/",
        json={
            "dish": "test_dish",
            "number_of_views": 1,
            "cooking_time": 99,
            "ingredients": "ingredients_1, ingredients_2",
            "description": "description of the test dish"
        }
    )
    assert response.status_code == 201


async def test_get_recipe(ac):
    response = await ac.get("/recipe/")
    assert response.status_code == 200
    assert response.json() == [
        {
            "dish": "test_dish",
            "number_of_views": 1,
            "cooking_time": 99,
        }
    ]


async def test_get_recipe_idx(ac):
    response = await ac.get("/recipe/1")
    assert response.status_code == 200
    assert response.json() == {
            "id": 1,
            "dish": "test_dish",
            "number_of_views": 2,
            "cooking_time": 99,
            "ingredients": "ingredients_1, ingredients_2",
            "description": "description of the test dish"
        }


async def test_get_recipe_idx_null(ac):
    response = await ac.get("/recipe/2")
    assert response.status_code == 200
    assert response.json() == None
