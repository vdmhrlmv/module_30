from typing import AsyncGenerator

from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker

from .logger import logger

DATABASE_URL = "sqlite+aiosqlite:///./app.db"

logger.info(f'Create engine. url={DATABASE_URL}')
engine = create_async_engine(DATABASE_URL, connect_args={"check_same_thread": False})
async_session_maker = sessionmaker(
        engine, expire_on_commit=False, class_=AsyncSession
    )


async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session
