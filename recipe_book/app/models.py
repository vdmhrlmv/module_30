from sqlalchemy import Column, String, Integer, MetaData
from sqlalchemy.ext.declarative import declarative_base
#from .database import Base


Base = declarative_base()
metadata = MetaData()

class Recipe(Base):
    __tablename__ = 'recipe'
    id = Column(Integer, primary_key=True, index=True)
    dish = Column(String, nullable=False)
    cooking_time = Column(Integer, default=0)
    number_of_views = Column(Integer, default=0)
    ingredients = Column(String, nullable=False)
    description = Column(String, nullable=True)

    def __repr__(self):
        return f"Recipe: {self.dish}, {self.cooking_time}, {self.number_of_views}, " \
               f"{self.ingredients}, {self.description}"

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
