from pydantic import BaseModel, Field
#from pydantic.class_validators import Optional


class BaseRecipe(BaseModel):
    dish: str
    number_of_views: int
    cooking_time: int = Field(
        ...,
        gt=0,
        le=240
    )


class BaseRecipeDetail(BaseRecipe):

    ingredients: str
    description: str #Optional[str] = None


class BaseRecipeDetailIn(BaseRecipeDetail):
    ...


class BaseRecipeDetailOut(BaseRecipeDetail):
    id: int

    class Config:
        orm_mode = True
