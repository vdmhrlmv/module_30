from sqlalchemy.future import select
from sqlalchemy import update
from sqlalchemy.ext.asyncio import AsyncSession

from .models import Recipe


async def update_views_count(session: AsyncSession, idx: int, new_count: int):
    query = (
        update(Recipe)
        .where(Recipe.id == idx)
        .values(number_of_views=new_count)
    )
    await session.execute(query)
    return True
