from typing import List, Union

import uvicorn as uvicorn
from fastapi import FastAPI, Depends
from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.logger import logger
from app.models import Base, Recipe
from app.schemas import BaseRecipe, BaseRecipeDetailOut, BaseRecipeDetailIn
from app.utils import update_views_count
from app.database import engine, get_async_session

# new comment

app = FastAPI()


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        logger.info('Create all')
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await engine.dispose()


@app.get('/')
async def root():
    return {
        'Service name': 'recipe_book'
    }


@app.get('/recipe/', response_model=List[BaseRecipe])
async def recipe(session: AsyncSession = Depends(get_async_session)):
    res = await session.execute(select(
        Recipe.dish,
        Recipe.number_of_views,
        Recipe.cooking_time).order_by(Recipe.number_of_views)
                                .order_by(Recipe.cooking_time)
                                 )
    await session.commit()
    return res.fetchall()


@app.post('/recipe/', response_model=BaseRecipeDetailOut, status_code=201)
async def recipe_add(recipe: BaseRecipeDetailIn, session: AsyncSession = Depends(get_async_session)) -> Recipe:
    new_recipe = Recipe(**recipe.dict())
    async with session.begin():
        session.add(new_recipe)
    return new_recipe


@app.get('/recipe/{idx}', response_model=Union[BaseRecipeDetailOut, None])
async def recipe_detail(idx: int, session: AsyncSession = Depends(get_async_session)) -> Recipe:
    response = await session.execute(
        select(Recipe).where(Recipe.id == idx)
    )
    result = response.scalars().one_or_none()
    if result:
        await update_views_count(session, idx, result.number_of_views + 1)
    await session.commit()
    return result


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5000)
